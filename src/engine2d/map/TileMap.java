package engine2d.map;

import engine2d.core.OnRenderEvent;
import engine2d.core.Rect;
import engine2d.core.Renderable;
import engine2d.core.TileSet;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Tile map rendering.
 * Handling moving screen bounding.
 * Handling collisions.
 * Handling layers.
 * OnRender is called when it's time to draw the 'externalsLayer', then you can handle z-index.
 * 1) setting externalsLayerIndex
 * 2) setting onRender callback
 * Updates can be mades in the main loop.
 *
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class TileMap implements Renderable {

    /**
     * Show collisions areas.
     */
    private static final boolean debug = false;

    /**
     * Cumulable velocity
     */
    private static final int VEL = 4;

    /**
     * Tile collisions in the map
     */
    public final List<Rect> collisions = new ArrayList<>();

    /**
     * Number of layers
     */
    protected int layersCount = 0;

    /**
     * Tiles to use for the map
     */
    protected TileSet tiles = null;

    /**
     * Map width (in tile)
     */
    public int width = 0;

    /**
     * Map height (in tile)
     */
    public int height = 0;

    /**
     * Screen width (how many tiles columns)
     */
    public int screenWidth = 32;

    /**
     * Screen height (how many tiles rows)
     */
    public int screenHeight = 32;

    /**
     * Tile's pixels width
     */
    public int tileWidth = 32;

    /**
     * Tile's pixels height
     */
    public int tileHeight = 32;

    /**
     * @todo: serialize to one D array
     * Layers [num of the layer][map width][map height]
     */
    protected int[][][] layers = null;

    /**
     *
     */
    protected final List<OnRenderEvent> layerRenderers = new ArrayList<>();
    
    /**
     * Real map x position (in pixels)
     */
    public double realX = 0;

    /**
     * Real map y position (in pixels)
     */
    public double realY = 0;

    /**
     * X velocity
     */
    public double velocityX = 0;

    /**
     * Y velocity
     */
    public double velocityY = 0;

    /**
     * 
     */
    //private LinkedList<Double> lm = new LinkedList<>();
    
    /**
     *
     * @param <T>
     * @param filename
     *
     * @return
     *
     * @throws Throwable
     */
    public abstract <T extends TileMap> T loadFromFile(String filename) throws Throwable;

    /**
     *
     * @return
     */
    public boolean isMoving() {
	return this.velocityX != 0 || this.velocityY != 0;
    }

    /**
     *
     * @param point
     */
    public void setPositionFromCenter(Point2D point) {
	realX = (point.getX() - (screenWidth / 2.0)) * -tileWidth;
	realY = (point.getY() - (screenHeight / 2.0)) * -tileHeight;
    }

    /**
     *
     * @param b
     */
    public void goRight(boolean b) {
	if (b) {
	    velocityX -= VEL;
	} else {
	    velocityX = 0;
	}
    }

    /**
     *
     * @param b
     */
    public void goUp(boolean b) {
	if (b) {
	    velocityY += VEL;
	} else {
	    velocityY = 0;
	}
    }

    /**
     *
     * @param b
     */
    public void goDown(boolean b) {
	if (b) {
	    velocityY -= VEL;
	} else {
	    velocityY = 0;
	}
    }

    /**
     *
     * @param b
     */
    public void goLeft(boolean b) {
	if (b) {
	    velocityX += VEL;
	} else {
	    velocityX = 0;
	}
    }

    /**
     *
     * @param elapsedTime
     */
    public void update(long elapsedTime, Rect rect) {
	if (isMoving()) {
	    //current position
	    double x = realX;
	    double y = realY;
	    //new position
	    realX += velocityX;
	    realY += velocityY;
	    //can update? if not, return to current position
	    for (Rect c : collisions) {
		if (c.intersect(new Rect(-realX + rect.getX(), -realY + rect.getY(), rect.getWidth(), rect.getHeight()))) {
		    realX = x;
		    realY = y;
		}
	    }
	}
    }

    /**
     *
     * @param <T>
     * @param point
     *
     * @return
     */
    @Override
    public <T extends Renderable> T setPosition(Point2D point) {
	return (T) this;
    }

    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	//Compute map window
	int mX = (int) -realX / tileWidth;
	int mY = (int) -realY / tileHeight;
	//Bounds to array
	if (mX < 0) {
	    mX = 0;
	}
	if (mX + screenWidth > this.width) {
	    mX = width - screenWidth;
	}
	if (mY < 0) {
	    mY = 0;
	}
	if (mY + screenHeight > this.height) {
	    mY = height - screenHeight;
	}
	//Render
	//long s = System.nanoTime();
	for (int l = 0; l < layersCount; l++) {
	    for (int y = mY; y < mY + this.screenHeight + 1; y++) {
		double ty = y * this.tileHeight;
		for (int x = mX; x < mX + this.screenWidth + 1; x++) {
		    double tx = x * this.tileWidth;
		    //Render background layer with 0's
		    if (l == 0) {
			this.tiles.render(gc, 0, tx + realX, ty + realY);
		    }
		    //Render current layer
		    this.tiles.render(gc, this.layers[l][x][y], tx + realX, ty + realY);
		    //
		    try{
			renderOnLayer(l).call(gc);
		    }catch(NullPointerException ex){
			
		    }
		}
	    }
	}
	/*
	lm.add(((System.nanoTime()-s)/1000.0));
	if(lm.size()>100){
	    lm.removeFirst();
	}
	double m = 0;
	for(Double d:lm){
	    m+=d;
	}	
	gc.strokeText("Render in: "+(m/lm.size())+"us",300,20);
	*/
	//Debug, draw collisions areas
	if (debug) {
	    for (Rect c : collisions) {
		gc.setStroke(Color.BLUE);
		gc.strokeRect(c.getX() + realX, c.getY() + realY, c.getWidth(), c.getHeight());
	    }
	}
    }
    
    /**
     * 
     * @param achar
     * @return 
     */
    public OnRenderEvent renderOnLayer(int layerIndex) {
	return layerRenderers.get(layerIndex);	
    }
    
    /**
     * 
     * @param layerName
     * @param onRenderEvent
     * @return 
     */
    public <T extends TileMap> T setLayerRenderer(int layerIndex, OnRenderEvent onRenderEvent) {
	layerRenderers.set(layerIndex, onRenderEvent);
	return (T) this;
    }
}

