package engine2d.core;

import javafx.geometry.Point2D;

/**
 * Tile (static).
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Tile {
    
    /**
     * 
     */
    private final Point2D size;
    
    /**
     * 
     */
    private final Point2D position;
    
    /**
     * 
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public Tile(int x, int y, int width, int height){
	this.position = new Point2D(x,y);
	this.size = new Point2D(width,height);
    }
    
    /**
     * 
     * @param position
     * @param size
     */
    public Tile(Point2D position, Point2D size){
	this.position = position;
	this.size = size;
    }
    
    /**
     * 
     * @return 
     */
    public Point2D getPosition(){
	return position;
    }
    
    /**
     * 
     * @return 
     */
    public Point2D getSize(){
	return size;
    }
}
