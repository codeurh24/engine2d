package engine2d.core;

/**
 * Object that can be updated in timed loop.
 * 
 * The update method will handle transformations/calculations of the object.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Updatable {

    /**
     * 
     * @param elapsedTime loop time in ms
     */
    void update(long elapsedTime);
}
