package engine2d.core;

/**
 * Go from 'start' to 'end' in 'time' ms. Interpolation is a double type.
 * Handle the negative values and 'end' value can be lower than 'start' value.
 * 
 * Interpolation is Animable, you can 'Play', 'Pause', 'Resume', 'Stop' it.
 * 
 * The 'update' method compute the value and return it.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Interpolation extends Animated{

    /**
     * Start value.
     */
    private double start;

    /**
     * Current value.
     */
    private double cur;

    /**
     * End value.
     */
    private double end;

    /**
     * Length between start and end value.
     */
    private double len;

    /**
     * Interpolation time.
     */
    private int duration;

    /**
     * Cumulated time from start.
     */
    private double cumul;

    /**
     * Empty Constructor.
     */
    public Interpolation() {
	this.start = 0;
	this.cur = 0;
	this.end = 0;
	this.cumul = 0;
	this.duration = 0;
	this.len = 0;
    }

    /**
     * Set the interpolation parameters.
     * 
     * @param start
     * @param end
     * @param duration
     */
    public final void set(double start, double end, int duration) {
	this.start = start;
	this.cur = start;
	this.end = end;
	this.cumul = 0;
	this.duration = duration;
	this.len = Math.abs(end - start);
    }

    /**
     * Constructor.
     * 
     * @param start
     * @param end
     * @param duration
     */
    public Interpolation(double start, double end, int duration) {
	set(start, end, duration);
    }

    /**
     * Update the interpolation value.
     * 
     * @todo: optimize
     * 
     * @param time time elapsed from last update
     * @return 
     */
    public double update(long time) {
	if (playing) {
	    cumul += time;
	    if (end > start) {
		if (cur < end) {
		    cur = start + (cumul / duration) * len;
		} else {
		    playing = false;
		    if (onEnd != null) {
			onEnd.call();
		    }
		    if (looping) {
			playing = true;
			cur = start;
			cumul = 0;
		    } else {
			cur = end;
		    }
		}
	    } else {
		if (cur > end) {
		    cur = start - (cumul / duration) * len;
		} else {
		    playing = false;
		    if (onEnd != null) {
			onEnd.call();
		    }
		    if (looping) {
			playing = true;
			cur = start;
			cumul = 0;
		    } else {
			cur = end;
		    }
		}
	    }
	}
	return cur;
    }
}
