package engine2d.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * <property name="screenheight" type="int" value="16"/>
 * <property name="screenwidth" type="int" value="16"/>
 *
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Property{

    /**
     * 
     */
    @XmlAttribute
    private String name;
    
    /**
     * 
     */
    @XmlAttribute
    private String value;
    
    /**
     * 
     * @return 
     */
    public String getName(){
	return this.name;
    }
    
    /**
     * 
     * @return 
     */
    public String getValue(){
	return this.value;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString(){
	return getName()+"="+getValue();
    }
}
