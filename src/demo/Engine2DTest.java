package demo;

import engine2d.core.AnimatedSprite;
import engine2d.core.Engine;
import engine2d.core.KeyFrames;
import engine2d.core.Sprite;
import engine2d.core.TileSet;
import engine2d.core.TileSetException;
import engine2d.core.text.Dialog;
import engine2d.core.text.LetterByLetterText;
import engine2d.map.tmx.MapException;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Engine2DTest extends Application {

    /**
     * 
     */
    private static final int DURATION = 128;
    
    /**
     * 
     */
    private Dialog dia;
    
    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {
	//Loading multiple tile sets
	TileSet t = new TileSet("Riou.png", 32, 48).setTransparent(0, 0);
	TileSet tb = new TileSet("Riou-Battle.png", 32, 64).setTransparent(0, 0);
	//New fixed sprite using a tile set
	Sprite sprite = new Sprite(t, 0, 0, 0);
	//New animated sprite using the tile sets
	AnimatedSprite d = new AnimatedSprite(t, 0, 0).addKeyFrames("DOWN", new KeyFrames(1, 0, 1, 2)).setDuration(DURATION).play(false);
	AnimatedSprite u = new AnimatedSprite(t, 48, 0).addKeyFrames("UP", new KeyFrames(13, 12, 13, 14)).setDuration(DURATION).play(true);
	AnimatedSprite l = new AnimatedSprite(t, 96, 0).addKeyFrames("LEFT", new KeyFrames(25, 24, 25, 26)).setDuration(DURATION).play(true);
	AnimatedSprite r = new AnimatedSprite(t, 128, 0).addKeyFrames("RIGHT", new KeyFrames(37, 36, 37, 38)).setDuration(DURATION).play(true);

	AnimatedSprite dr = new AnimatedSprite(t, 0, 48).addKeyFrames("DOWN_RUN", new KeyFrames(3, 4, 5, 6, 7, 8)).setDuration(DURATION).play(true);
	AnimatedSprite ur = new AnimatedSprite(t, 48, 48).addKeyFrames("UP_RUN", new KeyFrames(15, 16, 17, 18, 19, 20)).setDuration(DURATION).play(true);
	AnimatedSprite lr = new AnimatedSprite(t, 96, 48).addKeyFrames("LEFT_RUN", new KeyFrames(27, 28, 29, 30, 31, 32)).setDuration(DURATION).play(true);
	AnimatedSprite rr = new AnimatedSprite(t, 128, 48).addKeyFrames("RIGHT_RUN", new KeyFrames(39, 40, 41, 42, 43, 44)).setDuration(DURATION).play(true);

	AnimatedSprite rc = new AnimatedSprite(t, 150, 48).addKeyFrames("RUN_CLOUD", new KeyFrames(9, 10, 11, 21, 22, 23)).setDuration(DURATION).play(true);

	AnimatedSprite b = new AnimatedSprite(tb, 0, 96).addKeyFrames("BATTLE", new KeyFrames(0, 1, 2, 3)).setDuration(DURATION).play(true);
	
	//2D engine to render the tiles/sprites
	new Engine(theStage, 32 * 16, 32 * 16) {

	    @Override
	    public void initialize(GraphicsContext gc) {
		gc.setFont(Font.font("Menlo",12));		
		LetterByLetterText.getTextSize(gc);
		//
		dia = new Dialog(new Point2D(10,10),"Test de la boîte de dialogue. Il faut une ligne super grande pour voir comment elle sera coupée et il faut aussi tester le saut de ligne!\n A partir d'ici j'ai sauté une ligne\n deux lignes \n et même trois!!").play(false);
		this.addKeyable(dia);
	    }

	    @Override
	    public void update(GraphicsContext gc, long elapsedTime){
		gc.clearRect(0, 0, 32 * 16, 32 * 16);
		
		dia.update(elapsedTime);
		
		sprite.render(gc);
		
		d.render(gc);
		
		r.render(gc);
		l.render(gc);
		u.render(gc);

		dr.render(gc);
		rr.render(gc);
		lr.render(gc);
		ur.render(gc);

		rc.render(gc);

		b.render(gc);

		dia.render(gc);
	    }
	};
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	launch(args);
    }

}
