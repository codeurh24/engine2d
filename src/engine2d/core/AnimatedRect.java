package engine2d.core;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Animate the four points of a 2D rect (one interpolation for each).
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedRect extends Animated implements Renderable, Updatable {

    /**
     *
     */
    private final Interpolation x;

    /**
     *
     */
    private final Interpolation y;

    /**
     *
     */
    private final Interpolation w;

    /**
     *
     */
    private final Interpolation h;

    /**
     *
     */
    private double xp, yp, wp, hp;

    /**
     *
     * @param a
     * @param aa
     * @param b
     * @param bb
     * @param c
     * @param cc
     * @param d
     * @param dd
     */
    public AnimatedRect(double a, double aa, double b, double bb, double c, double cc, double d, double dd, int duration) {
	x = new Interpolation(a, aa, duration);
	y = new Interpolation(b, bb, duration);
	w = new Interpolation(c, cc, duration);
	h = new Interpolation(d, dd, duration);
    }

    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	gc.setStroke(Color.WHITE);
	gc.setFill(Color.BLACK);	
	gc.setLineWidth(2);
	gc.fillRoundRect(xp-2, yp-2, wp+4, hp+4, 14, 14);
	gc.strokeRoundRect(xp, yp, wp, hp, 10, 10);	
    }

    /**
     * 
     * @param <T>
     * @param point
     * @return 
     */
    @Override
    public <T extends Renderable> T setPosition(Point2D point) {
	return (T) this;
    }

    /**
     * 
     * @param elapsedTime
     */
    @Override
    public void update(long elapsedTime){
	if (isPlaying()) {
	    xp = x.update(elapsedTime);
	    yp = y.update(elapsedTime);
	    wp = w.update(elapsedTime);
	    hp = h.update(elapsedTime);
	    if(!x.isPlaying() && !y.isPlaying() && !w.isPlaying() && !h.isPlaying()){
		stop();
		onEnd();
	    }
	}
    }

    /**
     *
     * @param loop
     * @return 
     */
    @Override
    public AnimatedRect play(boolean loop) {
	x.play(false);
	y.play(false);
	w.play(false);
	h.play(false);
	return super.play(loop);
    }
    
    //@todo: pause, resume
}
