package engine2d.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * <layer name="back" width="128" height="128">
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Layer {

    /**
     *
     */
    @XmlAttribute
    private String name;

    /**
     *
     */
    @XmlAttribute
    private int width;

    /**
     *
     */
    @XmlAttribute
    private int height;

    /**
     *
     */
    @XmlElement
    private Data data;

    /**
     *
     * @return
     */
    public String getName() {
	return name;
    }

    /**
     *
     * @return
     */
    public int getWidth() {
	return width;
    }

    /**
     *
     * @return
     */
    public int getHeight() {
	return height;
    }

    /**
     *
     * @return
     */
    public Data getData() {
	return data;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
	return this.getName() + " " + this.getWidth() + "," + this.getHeight();
    }
}
