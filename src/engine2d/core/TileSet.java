package engine2d.core;

import engine2d.core.utils.ImageUtils;
import java.io.File;
import java.io.FileNotFoundException;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Tileset image accessible by each tile index from top,left to bottom right (row by row).
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSet {

    /**
     * Tile set image
     */
    private Image tileset;

    /**
     * Tile width
     */
    private final int tileWidth;

    /**
     * Tile height
     */
    private final int tileHeight;

    /**
     * Number of cols
     */
    private final int nbx;

    /**
     * Number of rows
     */
    private final int nby;

    /**
     * Create and load a tile set from a system file.
     *
     * @param filename   the tile set image filename
     * @param tileWidth  the tile width
     * @param tileHeight the tile height
     *
     * @throws java.io.FileNotFoundException
     * @throws engine2d.core.TileSetException
     */
    public TileSet(String filename, int tileWidth, int tileHeight) throws FileNotFoundException, TileSetException {
	if (tileWidth <= 0 || tileHeight <= 0) {
	    throw new TileSetException("Tile width or height can't be zero or negative");
	}
	this.tileWidth = tileWidth;
	this.tileHeight = tileHeight;
	File f = new File(filename);
	if (!f.exists()) {
	    throw new FileNotFoundException("TileSet file '" + filename + "' doesn't exists!");
	} else {
	    tileset = new Image("File:" + filename);
	    if (tileset.getWidth() < getTileWidth() || tileset.getHeight() < getTileHeight()) {
		throw new TileSetException("The tile set image (" + tileset.getWidth() + "x" + tileset.getHeight() + "pxs) is smaller than a tile (" + getTileWidth() + "x" + getTileHeight() + "pxs)");
	    }
	    this.nbx = (int) (this.tileset.getWidth() / getTileWidth());
	    this.nby = (int) (this.tileset.getHeight() / getTileHeight());
	}
    }

    /**
     * Set transparent color from a point in the tile set.
     *
     * Detect color at 'x,y' and rewrite image in memory with this color to transparent
     * Use setTransparent(Color) after the detection
     *
     * @param x X coordinate of the color to convert to transparent
     * @param y Y coordinate of the color to convert to transparent
     *
     * @return chaining
     */
    public TileSet setTransparent(int x, int y) {
	return setTransparent(ImageUtils.pickColor(tileset, x, y));
    }

    /**
     * Set the color 'col' to transparent.
     *
     * Rewrite image in memory with this color to transparent
     *
     * @param col Color to set to transparent
     *
     * @return chaining
     */
    public TileSet setTransparent(Color col) {
	tileset = ImageUtils.setTransparentColor(tileset, col);
	return this;
    }

    /**
     * Render the tile by its 'code' on the 'gc' at position 'x','y'.
     * If 'code' is not between 0 and the max tile number, no render is processed
     *
     * @param gc   GraphicsContext to draw on
     * @param code tile code to draw
     * @param x    gc x coordinate for where to draw the tile
     * @param y    gc y coordinate for where to draw the tile
     */
    public void render(GraphicsContext gc, int code, double x, double y) {
	if (code >= 0 && code < this.getNumberOfTiles()) {
	    Point2D p = new Point2D(this.getTileWidth() * (code % this.getNumberOfColumns()), this.getTileHeight() * (code / this.getNumberOfColumns()));
	    Tile t = new Tile(p,new Point2D(this.getTileWidth(), this.getTileHeight()));
	    render(gc,t,x,y);
	}
    }

    /**
     * 
     * @param gc
     * @param tile
     * @param x
     * @param y 
     */
    public void render(GraphicsContext gc, Tile tile, double x, double y) {
	gc.drawImage(this.tileset, tile.getPosition().getX(), tile.getPosition().getY(), tile.getSize().getX(), tile.getSize().getY(), x, y, tile.getSize().getX(), tile.getSize().getY());
    }

    /**
     *
     * @return the tile width
     */
    public final int getTileWidth() {
	return this.tileWidth;
    }

    /**
     *
     * @return the tile height
     */
    public final int getTileHeight() {
	return this.tileHeight;
    }

    /**
     *
     * @return number of tile in the tile set
     */
    public int getNumberOfTiles() {
	return getNumberOfColumns() * getNumberOfRows();
    }

    /**
     *
     * @return number of columns in the tile set
     */
    public int getNumberOfColumns() {
	return this.nbx;
    }

    /**
     *
     * @return number of rows in the tile set
     */
    public int getNumberOfRows() {
	return this.nby;
    }

    /**
     *
     * @return the tile set image
     */
    public Image getImage() {
	return this.tileset;
    }

    /**
     * Debug purpose.
     * Render the tile set to 'gc' with code on each tile.
     *
     * @param gc GraphicsContext to draw on
     */
    public void show(GraphicsContext gc) {
	int k = 0;
	gc.setFill(Color.WHITE);
	gc.setStroke(Color.BLACK);
	for (int y = 0; y < nby; y++) {
	    for (int x = 0; x < nbx; x++) {
		render(gc, k, x * tileWidth, y * tileHeight);
		gc.strokeText(k + "", x * tileWidth + 4, y * tileHeight + 16);
		gc.fillText(k + "", x * tileWidth + 4, y * tileHeight + 16);
		gc.strokeRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
		k++;
	    }
	}
    }
}
