package engine2d.core;

import engine2d.core.utils.LongValue;
import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Core javaFX canvas 2D engine.
 *
 * Create a scene with a full canvas in the constructor, add key listening.
 * Call initialize method with the GraphicsContext as parameter.
 * Create and start a javaFX AnimationTimer that call the update method (at 60 fps)
 * Show the form.
 *
 * Use:
 *
 * -Dprism.verbose=true -Dprism.order=es2,j2d -Dprism.forceGPU=true
 *
 * As run arguments to get OpenGL/DirectX hardware accelerated
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class Engine extends KeyHandler {

    /**
     * The engine GraphicsContext
     */
    private final GraphicsContext gc;

    /**
     * The engine Canvas
     */
    private final Canvas canvas;

    /**
     * Create a new engine on the Stage 's', giving the width and height
     *
     * @param s      Stage to use for rendering
     * @param width  width
     * @param height height
     */
    public Engine(Stage s, int width, int height) {
	//Setting the scene
	Group root = new Group();
	Scene theScene = new Scene(root);
	s.setScene(theScene);
	//Creating canvas on the scene
	canvas = new Canvas(width, height);
	root.getChildren().add(canvas);
	//Setting keyboard events
	theScene.setOnKeyPressed((KeyEvent e) -> {
	    onKeyPress(e);
	});
	theScene.setOnKeyReleased((KeyEvent e) -> {
	    onKeyUp(e);
	});
	//Getting the GraphicsContext
	gc = canvas.getGraphicsContext2D();
	//
	initialize(gc);
	//Time values
	LongValue lastNanoTime = new LongValue(System.nanoTime());

	//Rendering loop
	new AnimationTimer() {

	    @Override
	    public void handle(long currentNanoTime) {		
		// calculate time since last update.
		long elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000;
		//Callback update function with GraphicContext, times
		update(gc, elapsedTime);
		//Must be last
		lastNanoTime.value = currentNanoTime;
	    }
	}.start();
	//Set Stage width
	s.setWidth(width);
	//Set Stage height
	s.setHeight(height);
	//Show Stage
	s.show();
    }

    /**
     * Update callback which is called in each engine loop.
     *
     * @param gc
     * @param elapsedTime loop time in ms
     * @param fps
     */
    public abstract void update(GraphicsContext gc, long elapsedTime);

    /**
     * Initialize callback which is called after the GraphicsContext creation and before the engine loop
     *
     * @param gc
     */
    public abstract void initialize(GraphicsContext gc);
}
