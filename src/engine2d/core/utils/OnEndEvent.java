package engine2d.core.utils;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface OnEndEvent {
    
    /**
     * 
     */
    void call();
}
