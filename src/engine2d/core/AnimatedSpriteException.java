package engine2d.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedSpriteException extends Exception {

    /**
     * 
     * @param s 
     */
    public AnimatedSpriteException(String s) {
	super(s);
    }
    
}
