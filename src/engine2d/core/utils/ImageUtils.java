package engine2d.core.utils;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class ImageUtils {

    /**
     *
     * @param image
     * @param x
     * @param y
     *
     * @return
     */
    public static Color pickColor(Image image, int x, int y) {
	PixelReader pixelReader = image.getPixelReader();
	return pixelReader.getColor(x, y);
    }

    /**
     *
     * @param image
     * @param col
     *
     * @return
     */
    public static Image setTransparentColor(Image image, Color col) {
	PixelReader pixelReader = image.getPixelReader();
	// Create WritableImage
	WritableImage wImage = new WritableImage(
		(int) image.getWidth(),
		(int) image.getHeight());
	PixelWriter pixelWriter = wImage.getPixelWriter();
	// Determine the color of each pixel in a specified row
	for (int readY = 0; readY < image.getHeight(); readY++) {
	    for (int readX = 0; readX < image.getWidth(); readX++) {
		if (pixelReader.getColor(readX, readY).equals(col)) {
		    pixelWriter.setColor(readX, readY, Color.TRANSPARENT);
		} else {
		    pixelWriter.setColor(readX, readY, pixelReader.getColor(readX, readY));
		}
	    }
	}
	return wImage;
    }

}
