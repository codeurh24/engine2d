package engine2d.core;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.KeyEvent;

/**
 * Key handler based on Observer/Observable design pattern.
 * 
 * The key handler will dispatch key event to the listed keyables.
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class KeyHandler {

    /**
     *
     */
    protected final List<Keyable> keyables = new ArrayList<>();

    /**
     *
     * @param k
     */
    public void addKeyable(Keyable k) {
	keyables.add(k);
    }

    /**
     *
     * @param e
     */
    public void onKeyPress(KeyEvent e) {
	for (Keyable k : keyables) {
	    k.onKeyPress(e);
	}
    }

    /**
     * 
     * @param e 
     */
    public void onKeyUp(KeyEvent e) {
	for (Keyable k : keyables) {
	    k.onKeyUp(e);
	}
    }
}
