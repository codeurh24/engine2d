package engine2d.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSetException extends Exception {

    /**
     * 
     * @param s 
     */
    public TileSetException(String s) {
	super(s);
    }
    
}
