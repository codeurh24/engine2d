package engine2d.core.text;

import com.sun.javafx.tk.FontMetrics;
import com.sun.javafx.tk.Toolkit;
import engine2d.core.Keyable;
import engine2d.core.Renderable;
import engine2d.core.Updatable;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class LetterByLetterText implements Renderable, Updatable, Keyable {

    /**
     *
     */
    private static double letterWidth = 10;

    /**
     *
     */
    private static double letterHeight = 20;

    /**
     *
     */
    private int fastDuration = 0;

    /**
     *
     */
    private int duration = 112;

    /**
     *
     */
    private boolean fast = false;

    /**
     *
     */
    private final String text;

    /**
     *
     */
    private int col;

    /**
     *
     */
    private int line;

    /**
     *
     */
    private boolean playing = false;

    /**
     *
     */
    private Point2D position;

    /**
     *
     */
    private long previousTime;

    /**
     *
     */
    private final List<String> lines = new ArrayList<>();

    /**
     *
     * @param text
     * @param position
     * @param size
     */
    public LetterByLetterText(String text, Point2D position, Point2D size) {
	this.text = text;
	String[] ns = text.split("\n");
	int maxl = (int) Math.floor((size.getX()) / LetterByLetterText.letterWidth);
	for (String s : ns) {
	    int i = 0;
	    while (i < s.length()) {
		int m = maxl;
		if (m + i > s.length()) {
		    m = s.length() - i;
		}
		lines.add(s.substring(i, (m + i)).trim());
		i += m;
	    }
	}
	this.position = position;
    }

    /**
     *
     * @param gc
     */
    @Override
    public void render(GraphicsContext gc) {
	gc.setFill(Color.WHITE);
	double y = this.position.getY() + LetterByLetterText.letterHeight;
	if (this.line >= 1) {
	    for (int i = 0; i < this.line; i++) {
		gc.fillText(this.lines.get(i), position.getX(), y);
		y += LetterByLetterText.letterHeight;
	    }
	}
	if (line < lines.size() && col < text.length()) {
	    gc.fillText(this.lines.get(this.line).substring(0, this.col), this.position.getX(), y);
	}
    }

    /**
     * 
     * @param <T>
     * @param point
     * @return 
     */
    @Override
    public <T extends Renderable> T setPosition(Point2D point) {
	this.position = point;
	return (T) this;
    }

    /**
     *
     * @param elapsedTime
     */
    @Override
    public void update(long elapsedTime){
	if (this.isPlaying()) {
	    if (System.currentTimeMillis() - this.previousTime > (this.fast ? this.fastDuration : this.duration)) {
		this.col++;
		if (this.col > this.lines.get(line).length()) {
		    this.col = 0;
		    line++;
		    if (line > lines.size() - 1) {
//			waitUser();
			stop();
		    }
		}
		this.previousTime = System.currentTimeMillis();
	    }
	}
    }

    /**
     *
     * @return
     */
    public LetterByLetterText stop() {
	this.playing = false;
	return this;
    }

    /**
     *
     * @return
     */
    public LetterByLetterText play() {
	this.line = 0;
	this.col = 0;
	this.playing = true;
	this.previousTime = System.currentTimeMillis();
	return this;
    }

    /**
     *
     * @return
     */
    public boolean isPlaying() {
	return this.playing;
    }

    /**
     *
     */
    public void waitUser() {
	System.out.println("Plus de place, attend...");
    }

    /**
     *
     * @param gc
     */
    public static void getTextSize(GraphicsContext gc) {
	final FontMetrics fm = Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont());
	letterWidth = fm.computeStringWidth("T");
	letterHeight = fm.getLineHeight();
    }

    /**
     *
     */
    @Override
    public void onKeyPress(KeyEvent k) {
	fast = true;
    }

    /**
     *
     */
    @Override
    public void onKeyUp(KeyEvent k) {
	fast = false;
    }

}
